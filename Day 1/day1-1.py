most = 0

with open('input.txt', 'r', encoding='utf-8') as f:
    currentTotal = 0
    for line in f:
        if(line == '\n'):
            if currentTotal > most:
                most = currentTotal
            currentTotal = 0
        else:
            currentTotal += int(line)
print(most)