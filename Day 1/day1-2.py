most = [0,0,0]

def insert(num:int) -> bool:
    global most
    for i in range(0, len(most)):
        if(num > most[i]):
            for j in reversed(range(i, len(most))):
                    most[j] = most[j-1]
            most[i] = num
            return True
    return False

with open('input.txt', 'r', encoding='utf-8') as f:
    currentTotal = 0
    for line in f:
        if(line == '\n'):
            insert(currentTotal)
            currentTotal = 0
        else:
            currentTotal += int(line)
print(sum(most))