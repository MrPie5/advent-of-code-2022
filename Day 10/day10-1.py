cycle = 1
x = 1
totalStrength = 0
toAdd = None

def checkStrength():
    global totalStrength
    if ((cycle - 20) % 40) == 0:
        totalStrength += (x * cycle)

with open('input.txt', 'r', encoding='utf-8') as f:
    for line in f:

        checkStrength()

        if toAdd:
            x += toAdd
            toAdd = None
            cycle += 1
            checkStrength()

        if line.startswith('addx'):
            toAdd = int(line.split(' ', 1)[1])

        cycle += 1

print(totalStrength)