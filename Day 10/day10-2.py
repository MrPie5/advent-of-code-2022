cycle = 1
x = 1
toAdd = None

def draw():
    global x, cycle

    position = (cycle-1) % 40

    if x in range(position-1, position+2):
        print('#', end='')
    else:
        print('.', end='') 

    if cycle % 40 == 0:
        print('\n', end='')

with open('input.txt', 'r', encoding='utf-8') as f:
    for line in f:

        draw()
        if toAdd:
            x += toAdd
            toAdd = None
            cycle += 1
            draw()

        if line.startswith('addx'):
            toAdd = int(line.split(' ', 1)[1])

        cycle += 1