from __future__ import annotations # this is used for typeHinting classes
import operator
import re

monkeys = {}
symbols = {
    '+':operator.add,
    '*':operator.mul
}

class Monkey:
    def __init__(self, id:str, items:list, operation:function, operand:str, divBy:int, trueTarget:str, falseTarget:str):
        self.id = id
        self.items = items
        self.operation = operation
        self.operand = operand
        self.divBy = divBy
        self.trueTarget = trueTarget
        self.falseTarget = falseTarget
        self.inspectCount = 0

    def inspect(self) -> None:
        if len(self.items) == 0:
            return
        
        self.inspectCount += 1
        if self.operand == 'old':
            self.items[0] = self.operation(self.items[0], self.items[0])
        else:
            self.items[0] = self.operation(self.items[0], int(self.operand))

        self.items[0] = self.items[0] // 3
        target = self.trueTarget if (self.items[0] % self.divBy == 0) else self.falseTarget

        self.throw(target)

    def inspectAll(self) -> None:
        for _ in range(0, len(self.items)):
            self.inspect()

    def throw(self, target:str) -> None:
        monkeys[target].receive(self.items[0])
        del self.items[0]

    def receive(self, item:int) -> None:
        self.items.append(item)

    def __str__(self):
        return f'Monkey {self.id}:\n'\
            f'\tItems: {self.items}\n'\
            f'\tOperation: {self.operation}\n'\
            f'\tOperand: {self.operand}\n'\
            f'\tdivBy: {self.divBy}\n'\
            f'\ttrueTarget: {self.trueTarget}\n'\
            f'\tfalseTarget: {self.falseTarget}\n'\
            f'\tinspectCount: {self.inspectCount}'

with open('input.txt', 'r', encoding='utf-8') as f:
    blocks = f.read().split('\n\n')

    for block in blocks:
        info = block.split('\n')
    
        id = re.findall(r'(\d+)', info[0])[0]
        items = [int(a) for a in re.findall(r'(\d+)', info[1])]

        operation = symbols[re.findall(r'(\*|\+)', info[2])[0]]
        operand = re.findall(r'(\d+|old)', info[2])[1]
        divBy = int(re.findall(r'(\d+)', info[3])[0])
        trueTarget = re.findall(r'(\d+)', info[4])[0]
        falseTarget = re.findall(r'(\d+)', info[5])[0]

        monkeys[id] = Monkey(id, items, operation, operand, divBy, trueTarget, falseTarget)

for i in range(0, 20):

    for j in monkeys:
        monkeys[j].inspectAll()

highest = 0
nextHighest = 0

for j in monkeys:
    current = monkeys[j].inspectCount

    if current > highest:
        nextHighest = highest
        highest = current
    elif current > nextHighest:
        nextHighest = current

print(highest * nextHighest)