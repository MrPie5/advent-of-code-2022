total = 0

scoreKey = {
    'A': {
        'X': 3,
        'Y': 6,
        'Z': 0
    },
    'B': {
        'X': 0,
        'Y': 3,
        'Z': 6
    },
    'C': {
        'X': 6,
        'Y': 0,
        'Z': 3
    },
    'X': 1,
    'Y': 2,
    'Z': 3
}

with open('input.txt', 'r', encoding='utf-8') as f:
    for line in f:
        inp = line.strip().split(' ')
        total += scoreKey[inp[1]] + scoreKey[inp[0]][inp[1]]
print(total)
