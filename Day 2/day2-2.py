total = 0

scoreKey = {
    'A': {
        'X': 3,
        'Y': 1,
        'Z': 2
    },
    'B': {
        'X': 1,
        'Y': 2,
        'Z': 3
    },
    'C': {
        'X': 2,
        'Y': 3,
        'Z': 1
    },
    'X': 0,
    'Y': 3,
    'Z': 6
}

with open('input.txt', 'r', encoding='utf-8') as f:
    for line in f:
        inp = line.strip().split(' ')
        total += scoreKey[inp[1]] + scoreKey[inp[0]][inp[1]]
print(total)
