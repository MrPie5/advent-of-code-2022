import string

key = string.ascii_letters

total = 0

with open('input.txt', 'r', encoding='utf-8') as f:
    for line in f:
        line = line.rstrip()
        first = line[0:len(line)//2]
        second = line[len(line)//2:]
        for letter in first:
            if second.find(letter) != -1:
                total += key.find(letter) + 1
                break

print(total)