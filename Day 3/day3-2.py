import string

key = string.ascii_letters

total = 0

group = []

with open('input.txt', 'r', encoding='utf-8') as f:
    for line in f:
        group.append(line.rstrip())
        if len(group) == 3:
            for letter in group[0]:
                if group[1].find(letter) != -1 and group[2].find(letter) != -1:
                    total += key.find(letter) + 1
                    break
            group = []

print(total)