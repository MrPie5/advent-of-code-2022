total = 0

def expand(assRange:str) -> list:
    arr = assRange.split('-')
    output = [x for x in range(int(arr[0]), int(arr[1])+1)]
    return output

with open('input.txt', 'r', encoding='utf-8') as f:
    for line in f:
        assignments = []
        for assignment in line.rstrip().split(','):
            assignments.append(expand(assignment))

        # sort the assignments for easier checking
        assignments.sort(key = lambda x: len(x))

        # check if the longer list contains the first and last numbers of the shorter list
        if assignments[1].count(assignments[0][0]) and assignments[1].count(assignments[0][-1]):
            total += 1

print(total)