import re

def createStacks(line:str, stacks) -> None:
    for i in range(0, len(line)//4):
        stacks.append([])

def fillStacks(line:str, stacks:list) -> None:
    stackNum = 0
    for x in re.split(r'(.{4})', line.rstrip()):
        if x:
            if x.strip(' []'):
                stacks[stackNum].insert(0, x.strip(' []'))
            stackNum += 1

def moveStacks(line:str, stacks:list) -> None:
    instructions = re.split(r'move (\d+) from (\d+) to (\d+)', line)[1:-1]
    for i in range(0, int(instructions[0])):
        stacks[int(instructions[2])-1].insert( len(stacks[int(instructions[2])-1])-i, stacks[int(instructions[1])-1].pop())


with open('input.txt', 'r', encoding='utf-8') as f:
    phase = 0
    stacks = []
    for line in f:
        if not line.rstrip():
            continue

        if phase == 0:
            createStacks(line, stacks)
            fillStacks(line, stacks)
            phase += 1
        elif phase == 1:
            if re.search(r'^ 1', line):
                phase += 1
            else:
                fillStacks(line, stacks)
        elif phase == 2:
            moveStacks(line, stacks)

    output = ''
    for stack in stacks:
        output += stack[-1]
    print(output)