data = ''

with open('input.txt', 'r', encoding='utf-8') as f:
    data = f.read().rstrip()

sequenceLength = 4
counter = sequenceLength

valid = False

while counter < len(data):
    cur = data[counter-sequenceLength:counter]

    valid = True
    for c in cur:
        if cur.count(c) > 1:
            valid = False
            break
    if valid:
        print(cur)
        break
    else:
        counter += 1

print(counter)