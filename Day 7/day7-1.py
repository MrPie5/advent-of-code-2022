from __future__ import annotations # this is used for Node type-hinting itself

class Node:
    def __init__(self, name:str, parent:Node=None, size:int=0):
        self.name = name
        self.parent = parent
        self.children = []
        self.size = size

    def getSizeRecursive(self) -> int:
        totalSize = self.size
        for child in self.children:
            totalSize += child.getSizeRecursive()
        
        return totalSize

    def addChild(self, name:str, size:int=0) -> None:
        if self.findChild(name):
            return
        else:
            self.children.append(Node(name, parent=self, size=size))

    def findChild(self, name:str) -> Node:
        for child in self.children:
            if child.name == name:
                return child
        return None

    def sumUnderSize(self, size:int) -> int:
        total = 0
        mySize = self.getSizeRecursive()
        if mySize < size:
            total += mySize
        for child in self.children:
            if(len(child.children) > 0):
                total += child.sumUnderSize(size)
        return total

with open('input.txt', 'r', encoding='utf-8') as f:
    root = Node('/')

    cur = root

    for line in f:
        line = line.rstrip()
        if line.startswith('$'):
            if line[2:4] == 'cd':
                target = line.replace('$ cd ', '')
                if target == '/':
                    cur = root
                elif target == '..':
                    cur = cur.parent
                else:
                    cur = cur.findChild(target)
        elif line.startswith('dir'):
            cur.addChild(line.replace('dir ', ''))
        else:
            line = line.split(' ', 1)
            cur.addChild(line[1], size=int(line[0]))
    
    print(root.sumUnderSize(100000))