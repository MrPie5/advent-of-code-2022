rows = []
columns = []

# build the grid
with open('input.txt', 'r', encoding='utf-8') as f:
    for _ in range(0, len(f.readline().rstrip())):
        columns.append([])
    f.seek(0)

    i = 0
    for line in f:
        j = 0
        rows.append([])
        for num in line.rstrip():
            rows[i].append(int(num))
            columns[j].append(int(num))
            j += 1
        i += 1

visible = 0
rowIndex = 0

# walk interior rows
for row in rows[1:-1]:
    rowIndex += 1

    # walk interior trees within that row
    for i in range(1, len(row)-1):
        tree = row[i]

        tallest = 0
        checkColumn = True

        #check left and right
        for j in range(0, len(row)+1):
            if j == i or j == len(row):
                if tallest < tree:
                    visible += 1
                    checkColumn = False
                    break
                else:
                    tallest = 0
                    continue
            elif row[j] > tallest:
                tallest = row[j]

        # skip the column checking and proceed to the next iteration of the loop
        # if we already determined the tree is visible
        if not checkColumn:
            continue

        #check up and down
        column = columns[i]
        for j in range(0, len(column)+1):
            if j == rowIndex or j == len(column):
                if tallest < tree:
                    visible += 1
                    break
                else:
                    tallest = 0
                    continue
            elif column[j] > tallest:
                tallest = column[j]

# add the exterior trees
visible += (len(rows) * 2) + (len(columns) * 2) - 4
            
print(visible)