rows = []
columns = []

# build the grid
with open('input.txt', 'r', encoding='utf-8') as f:
    for _ in range(0, len(f.readline().rstrip())):
        columns.append([])
    f.seek(0)

    i = 0
    for line in f:
        j = 0
        rows.append([])
        for num in line.rstrip():
            rows[i].append(int(num))
            columns[j].append(int(num))
            j += 1
        i += 1

highest = 0
rowIndex = -1

# walk rows
for row in rows:
    rowIndex += 1

    # walk trees within that row
    for i in range(0, len(row)):
        score = [0,0,0,0]
        tree = row[i]


        #check left
        for j in reversed(range(0, i)):
            score[0] += 1
            if row[j] >= tree:
                break

        #check right
        for j in range(i+1, len(row)):
            score[1] += 1
            if row[j] >= tree:
                break

        column = columns[i]

        #check up
        for j in reversed(range(0, rowIndex)):
            score[2] += 1
            if column[j] >= tree:
                break
    
        #check down
        for j in range(rowIndex+1, len(column)):
            score[3] += 1
            if column[j] >= tree:
                break

        total = score[0] * score[1] * score[2] * score[3]

        if total > highest:
            highest = total
            highestArr = score
            position = [rowIndex, i]

print(highest)