tail, head = [0,0], [0,0]

tailVisited = ['0-0']

with open('input.txt', 'r', encoding='utf-8') as f:

    def sign(v:int) -> int:
        return -1 if v < 0 else 1

    for line in f:
        direction = line.split(' ')[0].lower()
        value = int(line.split(' ')[1])
        for i in range(0, value):
            if direction == 'u':
                head[1] += 1
            elif direction == 'd':
                head[1] -= 1
            elif direction == 'r':
                head[0] += 1
            elif direction == 'l':
                head[0] -= 1

            xdif = head[0] - tail[0]
            ydif = head[1] - tail[1]

            if abs(xdif) + abs(ydif) > 2:
                tail[0] += sign(xdif)
                tail[1] += sign(ydif)
            else:
                if abs(xdif) > 1:
                    tail[0] += (xdif - sign(xdif))

                if abs(ydif) > 1:
                    tail[1] += (ydif - sign(ydif))


            tailPosition = f'{tail[0]}-{tail[1]}'
            if tailPosition not in tailVisited:
                tailVisited.append(tailPosition)

print(len(tailVisited))