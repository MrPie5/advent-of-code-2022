knots = [[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]]

head, tail = knots[0], knots[-1]

tailVisited = ['0-0']

with open('input.txt', 'r', encoding='utf-8') as f:

    def sign(v:int) -> int:
        return -1 if v < 0 else 1

    for line in f:
        direction = line.split(' ')[0].lower()
        value = int(line.split(' ')[1])
        for _ in range(0, value):
            if direction == 'u':
                head[1] += 1
            elif direction == 'd':
                head[1] -= 1
            elif direction == 'r':
                head[0] += 1
            elif direction == 'l':
                head[0] -= 1

            for i in range(1, len(knots)):
                knot = knots[i]
                forward = knots[i-1]

                xdif = forward[0] - knot[0]
                ydif = forward[1] - knot[1]

                if abs(xdif) + abs(ydif) > 2:
                    knot[0] += sign(xdif)
                    knot[1] += sign(ydif)
                else:
                    if abs(xdif) > 1:
                        knot[0] += (xdif - sign(xdif))

                    if abs(ydif) > 1:
                        knot[1] += (ydif - sign(ydif))

            tailPosition = f'{tail[0]}-{tail[1]}'
            if tailPosition not in tailVisited:
                tailVisited.append(tailPosition)

print(len(tailVisited))